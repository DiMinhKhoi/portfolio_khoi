import React from "react";
import classNames from "classnames/bind";
import styles from "./About.module.scss";
import images from "../../assets/img";
import { useTranslation } from 'react-i18next';

const cx = classNames.bind(styles);

const About = () => {
    const { t } = useTranslation()

    const data =  [
        {
            name: "HTML5"
        },
        {
            name:"CSS/SCSS"
        },
        {
            name:"JAVASCRIPT"
        },
        {
            name:"WORDPRESS"
        },
        {
            name:"REACTJS"
        },
        {
            name:"DESIGN UI/UX"
        }
    ]

    return (
        <main className={cx("about-page")}>
            <div className="wrapper">
                <div className="title-page p-ani-fadeInUp p-delay_1s">{t('main.about_page.title_page')}</div>
                <div className="sub-title-page p-ani-fadeInUp p-delay_3s">{t('main.about_page.sub_title_page')}</div>

                <div className="info-box">
                    <div className={cx("flex", "flex-justify")}>
                        <figure className="p-ani-fadeInLeft p-delay_3s">
                            <img src={images.man} alt="man-avatar" />
                        </figure>
                        <div className={cx("introduce")}>
                            <h3 className="p-ani-fadeInRight p-delay_6s">
                                {t('main.about_page.introduce_about_me1')}
                            </h3>
                            <ul className="p-ani-fadeInRight p-delay_9s">
                                <li>
                                    {t('main.about_page.introduce_about_me2.title_birthday')}: <span>{t('main.about_page.introduce_about_me2.date_birthday')}</span>
                                </li>
                                <li>
                                    {t('main.about_page.introduce_about_me2.title_age')}: <span>23</span>
                                </li>
                                <li>
                                    Github: <a href="https://gitlab.com/DiMinhKhoi" target="_blank" rel="noreferrer"><span>https://gitlab.com/DiMinhKhoi</span></a>
                                </li>
                                <li>
                                    {t('main.about_page.introduce_about_me2.title_degree')}: <span>{t('main.about_page.introduce_about_me2.vocational_degree')}</span>
                                </li>
                                <li>
                                    {t('main.about_page.introduce_about_me2.title_phone')}: <span>0352862381</span>
                                </li>
                                <li>
                                    Email: <span>minhkhoi9a6@gmail.com</span>
                                </li>
                                <li>
                                    {t('main.about_page.introduce_about_me2.title_city')}: <span>{t('main.about_page.introduce_about_me2.name_city')}</span>
                                </li>
                            </ul>

                            <h4 className="bor-right p-ani-fadeInRight p-delay_12s">{t('main.about_page.skill')}</h4>
                            <div className={cx('skill-man', 'flex', "p-ani-fadeInRight", "p-delay_12s")}>
                                {data.map((data) => (
                                    <div className={cx('pro')}>
                                        <div className={cx('title-percent')}>
                                            <h5>{data.name}</h5>
                                        </div>
                                    {/* <div className="progress progress-striped">
                                        <div className="progress-bar"></div>
                                    </div> */}
                                    </div>
                                ))}                              
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    );
};

export default About;
